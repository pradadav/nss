function drawNavbar() {


    $.get("./header.html", function(data){
        $("#header").replaceWith(data);
    });

    $.get("./navbar.html", function(data){
        $("#navigation").replaceWith(data);
    });

}

function getCars(){
    $.ajax({
        url: "http://localhost:8080/nss_semestral_project_war_exploded/rest/cars/"
    }).then(function(response) {

        for(var i =0;i <= response.length-1;i++)
        {
            var item = response[i];

            var categoryString = "";

            for (var j =0;j <= item.categories.length-1;j++){
                var itemsec = item.categories[j];


                if (j != item.categories.length - 1){
                    categoryString += itemsec.name;
                    categoryString += ", ";

                }else {
                    categoryString += itemsec.name;
                }
            }


            var data = '<div class="row mt-3">\n' +
                '    <div class="col-8 offset-2 bg-light">\n' +
                '        <div class="row shadow-lg">\n' +
                '            <div class="col-4">\n' +
                '                <h4 class="mt-1 name">'+item.manufacturer.name+' '+ item.model  +' </h4>\n' +
                '                <p class="seats">Seats: '+ item.seats +'</p>\n' +
                '                <p class="category">'+categoryString+'</p>\n' +
                '            </div>\n' +
                '            <div class="col-4 offset-4">\n' +
                '                <h4 class="text-center mt-4 price">'+item.price+' CZK / day</h4>\n' +
                '                <button class="btn btn-primary btn-lg btn-block mt-2 rent" type="submit" id="'+item.id+'">Rent</button>\n' +
                '            </div>\n' +
                '        </div>\n' +
                '    </div>\n' +
                '</div>\n' +
                '<div class="carlist"></div>'

            $('.carlist').replaceWith(data);

        }

    });
}

function login (){
    var inputUsername = document.getElementById("username").value;
    var inputPassword = document.getElementById("password").value;

    console.log(inputUsername);
    console.log(inputPassword);
    //var loginURL = "http://192.168.0.157:8080/nss_semestral_project_war_exploded/j_spring_security_check?username=pradadav&password=admin";
    $.ajax({
        type: "POST",
        url: "http://192.168.0.157:8080/nss_semestral_project_war_exploded/j_spring_security_check?username=pradadav&password=admin"
    }).then(function(response) {
        console.log(response);
    });
}

function logout (){

    console.log("LOGOUT");
    $.ajax({
        type: "POST",
        url: "http://192.168.0.157:8080/nss_semestral_project_war_exploded/j_spring_security_logout"
    }).then(function(response) {
        console.log(response);
    });
}


/*$(document).ready(function() {



    $.ajax({
        url: "http://localhost:9999/nss_semestral_project_war_exploded/rest/cars/"
    }).then(function(response) {

        for(var i =0;i <= response.length-1;i++)
        {
            var item = response[i];

            var categoryString = "";

            for (var j =0;j <= item.categories.length-1;j++){
                var itemsec = item.categories[j];


                if (j != item.categories.length - 1){
                    categoryString += itemsec.name;
                    categoryString += ", ";

                }else {
                    categoryString += itemsec.name;
                }
            }


            var data = '<div class="row mt-3">\n' +
                '    <div class="col-8 offset-2 bg-light">\n' +
                '        <div class="row shadow-lg">\n' +
                '            <div class="col-4">\n' +
                '                <h4 class="mt-1 name">'+item.manufacturer.name+' '+ item.model  +' </h4>\n' +
                '                <p class="seats">Seats: '+ item.seats +'</p>\n' +
                '                <p class="category">'+categoryString+'</p>\n' +
                '            </div>\n' +
                '            <div class="col-4 offset-4">\n' +
                '                <h4 class="text-center mt-4 price">'+item.price+' CZK / day</h4>\n' +
                '                <button class="btn btn-primary btn-lg btn-block mt-2 rent" type="submit" id="'+item.id+'">Rent</button>\n' +
                '            </div>\n' +
                '        </div>\n' +
                '    </div>\n' +
                '</div>\n' +
                '<div class="carlist"></div>'

            $('.carlist').replaceWith(data);

        }

    });
});*/