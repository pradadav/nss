package dao;


import model.Manufacturer;
import org.springframework.stereotype.Repository;

@Repository
public class ManufacturerDao extends BaseDao<Manufacturer> {

    public ManufacturerDao() {
        super(Manufacturer.class);
    }
}
