package dao;

import model.Car;
import model.Category;
import model.Manufacturer;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Objects;

@Repository
public class CarDao extends BaseDao<Car> {

    public CarDao() {
        super(Car.class);
    }

    @Override
    public List<Car> findAll() {
        return em.createQuery("SELECT c FROM Car c WHERE NOT c.removed AND NOT c.rented", Car.class).getResultList();
    }

    public List<Car> findAll(Category categoty) {
        Objects.requireNonNull(categoty);
        return em.createNamedQuery("Car.findByCategory", Car.class).setParameter("category", categoty)
                .getResultList();
    }

    public List<Car> findAll(Manufacturer manufacturer) {
        Objects.requireNonNull(manufacturer);
        return em.createNamedQuery("Car.findByManufacturer", Car.class).setParameter("manufacturer", manufacturer)
                .getResultList();
    }
}
