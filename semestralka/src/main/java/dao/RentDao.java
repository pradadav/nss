package dao;

import model.Car;
import model.Rent;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RentDao extends BaseDao<Rent> {

    public RentDao() {
        super(Rent.class);
    }

}