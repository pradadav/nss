package model;

import java.io.Serializable;

public class Info implements Serializable {

    int carId;
    int year;
    int month;
    int day;

    public int getCarId() {
        return carId;
    }

    public int getYear() {
        return year;
    }

    public int getMonth() {
        return month;
    }

    public int getDay() {
        return day;
    }
}
