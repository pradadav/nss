package model;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


@Entity
@NamedQueries({
        @NamedQuery(name = "Car.findByCategory", query = "SELECT c from Car c WHERE :category MEMBER OF c.categories"),
        @NamedQuery(name = "Car.findByManufacturer", query = "SELECT c from Car c WHERE c.manufacturer = :manufacturer")
})
public class Car extends AbstractEntity {

    @Basic(optional = false)
    @Column(nullable = false)
    private Double price;

    @ManyToMany
    @OrderBy("name")
    private List<Category> categories;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Manufacturer manufacturer;

    @Basic(optional = false)
    @Column(nullable = false)
    @Size(min=1)
    private String model;

    @Basic(optional = false)
    @Column(nullable = false)
    private int seats;

    private Boolean removed = false;

    private Boolean rented = false;

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Boolean isRented() {
        return rented;
    }

    public void setRented(Boolean removed) {
        this.rented = removed;
    }

    public Manufacturer getManufacturer() { return manufacturer; }

    public void setManufacturer(Manufacturer manufacturer) { this.manufacturer = manufacturer; }

    public Boolean isRemoved() {
        return removed;
    }

    public void setRemoved(Boolean removed) {
        this.removed = removed;
    }

    public void addCategory(Category category) {
        Objects.requireNonNull(category);
        if (getCategories() == null) {
            this.setCategories(new ArrayList<>());
        }
        getCategories().add(category);
    }

    public void removeCategory(Category category) {
        Objects.requireNonNull(category);
        if (getCategories() == null) {
            return;
        }
        getCategories().removeIf(c -> Objects.equals(c.getId(), category.getId()));
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    @Override
    public String toString() {
        return "Car{" +
                "id='" + getId() +
                ", manufacturer='" + manufacturer +
                ", model=" + model +
                ", category=" + getCategories() +
                ", price=" + price +
                ", seats=" + seats +
                ", rented=" + rented +
                ", removed=" + removed +
                "}";
    }
}