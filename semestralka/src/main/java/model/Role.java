package model;



public enum Role {
    WORKER("ROLE_WORKER"), CUSTOMER("ROLE_CUSTOMER");

    private final String name;

    Role(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
