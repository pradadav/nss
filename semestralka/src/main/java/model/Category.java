package model;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
public class Category extends AbstractEntity {

    @Basic(optional = false)
    @Column(nullable = false)
    @Size(min=2)
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) { this.name = name; }


    @Override
    public String toString() {
        return "Category{" +
                "name='" + name + '\'' +
                "}";
    }

}