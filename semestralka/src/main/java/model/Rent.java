package model;


import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;


@Entity
@Table(name = "RENT")
public class Rent extends AbstractEntity {

    private LocalDate created;

    @Basic(optional = false)
    @Column(nullable = false)
    private LocalDate date_from;

    @Basic(optional = false)
    @Column(nullable = false)
    private LocalDate date_to;

    @ManyToOne
    @JoinColumn(nullable = false)
    private User user;


    @ManyToOne
    @JoinColumn(nullable = false)
    private Car car;

    @Basic(optional = false)
    @Column(nullable = false)
    private Double price;

    @Basic(optional = false)
    @Column(nullable = false)
    private Boolean canceled;

    public Rent() {
    }

    public Rent(Car car, User user, LocalDate date_from, LocalDate date_to) {
        this.user = user;
        this.car = car;
        this.date_from = date_from;
        this.date_to = date_to;
        this.canceled = false;
    }

    public LocalDate getCreated() {
        return created;
    }

    public void setCreated(LocalDate created) {
        this.created = created;
    }

    public User getUser() { return user; }

    public void setUser(User user) {
        this.user = user;
    }

    public Car getCar() { return car; }

    public void setCar(Car car) {
        this.car = car;
    }

    public Boolean getCanceled() {
        return canceled;
    }

    public void setCanceled(Boolean canceled) {
        this.canceled = canceled;
    }

    @Override
    public String toString() {
        return "Rent{" +
                "created=" + created +
                ", user=" + user +
                ", car=" + car +
                ", date_from=" + date_from.toString() +
                ", date_to=" + date_to.toString() +
                ", price=" + price.toString() +
                ", canceled=" + canceled+
                "}";
    }

    public LocalDate getDate_from() {
        return date_from;
    }

    public void setDate_from(LocalDate date_from) {
        this.date_from = date_from;
    }

    public LocalDate getDate_to() {
        return date_to;
    }

    public void setDate_to(LocalDate datum_do) {
        this.date_to = datum_do;
    }

    public Double getPrice() { return price; }

    public void setPrice(Double price) {
        this.price = price;
    }
}
