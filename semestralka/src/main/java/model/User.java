package model;

import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name = "NSS_USER")
@NamedQueries({
        @NamedQuery(name = "User.findByUsername", query = "SELECT u FROM User u WHERE u.username = :username"),
        @NamedQuery(name = "User.deleteByUsername", query = "DELETE FROM User u WHERE u.username = :username")
})
public class User extends AbstractEntity {

    @Basic(optional = false)
    @Column(nullable = false)
    @Size(min=2)
    private String firstName;

    @Basic(optional = false)
    @Column(nullable = false)
    @Size(min=2)
    private String lastName;

    @Basic(optional = false)
    @Column(nullable = false, unique = true)
    @Size(min=3)
    private String username;

    @Basic(optional = false)
    @Column(nullable = false)
    @Size(min=5)
    private String password;

    @Basic
    @Column
    private String email;

    @Enumerated(EnumType.STRING)
    private Role role;

    public User() {
        this.role = Role.CUSTOMER;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void encodePassword(PasswordEncoder encoder) {
        this.password = encoder.encode(password);
    }

    public void erasePassword() {
        this.password = null;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "User{" +
                firstName + " " + lastName +
                "(" + username + ")"+email+" }";
    }

    public String getMail() {
        return email;
    }

    public void setMail(String email) {
        this.email = email;
    }
}
