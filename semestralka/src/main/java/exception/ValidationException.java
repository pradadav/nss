package exception;


public class ValidationException extends MyException {

    public ValidationException(String message) {
        super(message);
    }
}
