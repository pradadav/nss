package exception;

/**
 * Indicates that insufficient amount of a product is available for processing, e.g. for creating order items.
 */
public class InsufficientAmountException extends MyException {

    public InsufficientAmountException(String message) {
        super(message);
    }
}
