package rest;

import exception.NotFoundException;
import exception.ValidationException;
import model.Car;
import model.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import rest.util.RestUtils;
import service.CarService;
import service.CategoryService;

import java.util.List;

@RestController
@RequestMapping("/categories")
public class CategoryController {

    private static final Logger LOG = LoggerFactory.getLogger(CategoryController.class);

    private final CategoryService service;

    private final CarService carService;

    @Autowired
    public CategoryController(CategoryService service, CarService carService) {
        this.service = service;
        this.carService = carService;
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Category> getCategories() {
        return service.findAll();
    }

    @PreAuthorize("hasRole('ROLE_WORKER')")
    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createCategory(@RequestBody Category category) {
        service.persist(category);
        LOG.debug("Created category {}.", category);
        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{id}", category.getId());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Category getById(@PathVariable("id") Integer id) {
        final Category category = service.find(id);
        if (category == null) {
            throw NotFoundException.create("Category", id);
        }
        return category;
    }

    @RequestMapping(value = "/{id}/cars", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Car> getCarsByCategory(@PathVariable("id") Integer id) {
        return carService.findAll(getById(id));
    }

    @PreAuthorize("hasRole('ROLE_WORKER')")
    @RequestMapping(value = "/{id}/cars", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void addCarToCategory(@PathVariable("id") Integer id, @RequestBody Car car) {
        final Category category = getById(id);
        service.addCar(category, car);
        LOG.debug("Car {} added into category {}.", car, category);
    }

    @PreAuthorize("hasRole('ROLE_WORKER')")
    @RequestMapping(value = "/{categoryId}/cars/{carId}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeCarFromCategory(@PathVariable("categoryId") Integer categoryId,
                                          @PathVariable("carId") Integer carId) {
        final Category category = getById(categoryId);
        final Car toRemove = carService.find(carId);
        if (toRemove == null) {
            throw NotFoundException.create("Car", carId);
        }
        service.removeCar(category, toRemove);
        LOG.debug("Car {} removed from category {}.", toRemove, category);
    }

    @PreAuthorize("hasRole('ROLE_WORKER')")
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateCategory(@PathVariable("id") Integer id, @RequestBody Category category) {
        final Category original = getById(id);
        if (!original.getId().equals(category.getId())) {
            throw new ValidationException("Car identifier in the data does not match the one in the request URL.");
        }
        service.update(category);
        LOG.debug("Updated category {}.", category);
    }

}
