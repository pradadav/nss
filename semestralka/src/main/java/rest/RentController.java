package rest;

import exception.NotFoundException;

import model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import rest.util.RestUtils;
import security.model.AuthenticationToken ;
import service.RentService;

import java.security.Principal;
import java.time.LocalDate;
import java.util.List;


@RestController
@RequestMapping("/rents")
@PreAuthorize("permitAll()")
public class RentController {

    private final RentService rentService;
    private final CarController carController;
    private final UserController userController;

    private static final Logger LOG = LoggerFactory.getLogger(RentController.class);

    @Autowired
    public RentController(RentService rentService, CarController carController, UserController userController) {
        this.rentService = rentService;
        this.carController = carController;
        this.userController = userController;
    }

    @PostFilter("hasRole('ROLE_ADMIN') or (filterObject.user.username == principal.username)")
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Rent> getRents() {
        return rentService.findAll();
    }

    @PreAuthorize("hasAnyRole('ROLE_WORKER', 'ROLE_CUSTOMER')")
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Void> createRent(Principal principal, @RequestBody Info info)
    {
        LocalDate date = LocalDate.of(info.getYear(),info.getMonth(),info.getDay());
        final Car car = carController.getCar(info.getCarId());
        final User user = userController.getCurrent(principal);
        final Rent finalRent = rentService.create(car,user,date);
        LOG.debug("Created rent {}.", finalRent);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PreAuthorize("hasAnyRole('ROLE_WORKER', 'ROLE_CUSTOMER')")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void endRent(Principal principal, @PathVariable("id") Integer id) {
        final Rent rent = getRent(principal,id);
        if (rent == null) {
            throw NotFoundException.create("Rent", id);
        }
        rentService.end(rent);
        LOG.debug("Canceled rent {}.", rent);
    }

    @PreAuthorize("hasAnyRole('ROLE_WORKER', 'ROLE_CUSTOMER')")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Rent getRent(Principal principal, @PathVariable("id") Integer rentId) {
        final Rent rent = rentService.find(rentId);
        if (rent == null) {
            throw NotFoundException.create("Rent", rentId);
        }
        final AuthenticationToken auth = (AuthenticationToken) principal;
        if (auth == null){
            return rent;
        }
        if (auth.getPrincipal().getUser().getRole() != Role.WORKER &&
                !rent.getUser().getId().equals(auth.getPrincipal().getUser().getId())) {
            throw new AccessDeniedException("Cannot access rent of another customer");
        }
        return rent;
    }
}


