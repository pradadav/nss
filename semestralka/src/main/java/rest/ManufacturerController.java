package rest;

import exception.NotFoundException;
import model.Car;
import model.Manufacturer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import rest.util.RestUtils;
import service.CarService;
import service.ManufacturerService;

import java.util.List;

@RestController
@RequestMapping("/manufacturers")
public class ManufacturerController {

    private static final Logger LOG = LoggerFactory.getLogger(CategoryController.class);

    private final ManufacturerService service;

    private final CarService carService;

    @Autowired
    public ManufacturerController(ManufacturerService service, CarService carService) {
        this.service = service;
        this.carService = carService;
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Manufacturer> getManufacturers() {
        return service.findAll();
    }

    @PreAuthorize("hasRole('ROLE_WORKER')")
    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createManufacturer(@RequestBody Manufacturer manufacturer) {
        service.persist(manufacturer);
        LOG.debug("Created manufacturer {}.", manufacturer);
        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{id}", manufacturer.getId());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Manufacturer getById(@PathVariable("id") Integer id) {
        final Manufacturer manufacturer = service.find(id);
        if (manufacturer == null) {
            throw NotFoundException.create("Manufacturer", id);
        }
        return manufacturer;
    }

    @RequestMapping(value = "/{id}/cars", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Car> getCarsByManufacturer(@PathVariable("id") Integer id) {
        return carService.findAllMan(getById(id));
    }
}
