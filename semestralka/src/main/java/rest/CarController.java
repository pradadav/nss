package rest;


import exception.NotFoundException;
import exception.ValidationException;
import model.Car;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import rest.util.RestUtils;
import service.CarService;

import java.util.List;

@RestController
@RequestMapping("/cars")
public class CarController {

    private static final Logger LOG = LoggerFactory.getLogger(CarController.class);

    private final CarService carService;

    @Autowired
    public CarController(CarService carService) {
        this.carService = carService;
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Car> getCars() {
        return carService.findAll();
    }

    @PreAuthorize("hasRole('ROLE_WORKER')")
    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createCar(@RequestBody Car car) {
        carService.persist(car);
        LOG.debug("Created car {}.", car);
        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{id}", car.getId());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Car getCar(@PathVariable("id") Integer id) {
        final Car c = carService.find(id);
        if (c == null) {
            throw NotFoundException.create("Car", id);
        }
        return c;
    }

    @PreAuthorize("hasRole('ROLE_WORKER')")
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateCar(@PathVariable("id") Integer id, @RequestBody Car car) {
        final Car original = getCar(id);
        if (!original.getId().equals(car.getId())) {
            throw new ValidationException("Car identifier in the data does not match the one in the request URL.");
        }
        carService.update(car);
        LOG.debug("Updated car {}.", car);
    }

    @PreAuthorize("hasRole('ROLE_WORKER')")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeCar(@PathVariable("id") Integer id) {
        final Car toRemove = carService.find(id);
        if (toRemove == null) {
            return;
        }
        carService.remove(toRemove);
        LOG.debug("Removed car {}.", toRemove);
    }
}
