package util;

import model.Role;

public final class Constants {

    /**
     * UTF-8 encoding identifier.
     */
    public static final String UTF_8_ENCODING = "UTF-8";

    /**
     * Default user role.
     */
    public static final Role DEFAULT_ROLE = Role.CUSTOMER;

    private Constants() {
        throw new AssertionError();
    }
}
