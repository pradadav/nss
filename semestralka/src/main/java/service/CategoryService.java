package service;


import dao.CarDao;
import dao.CategoryDao;
import model.Car;
import model.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

@Service
public class CategoryService {

    private final CategoryDao dao;

    private final CarDao carDao;

    @Autowired
    public CategoryService(CategoryDao dao, CarDao carDao) {
        this.dao = dao;
        this.carDao = carDao;
    }

    @Transactional(readOnly = true)
    public List<Category> findAll() {
        return dao.findAll();
    }

    @Transactional(readOnly = true)
    public Category find(Integer id) {
        return dao.find(id);
    }

    @Transactional
    public void persist(Category category) {
        Objects.requireNonNull(category);
        dao.persist(category);
    }

    @Transactional
    public void update(Category category) {
        dao.update(category);
    }

    @Transactional
    public void addCar(Category category, Car car) {
        Objects.requireNonNull(category);
        Objects.requireNonNull(car);
        car.addCategory(category);
        carDao.update(car);
    }

    @Transactional
    public void removeCar(Category category, Car car) {
        Objects.requireNonNull(category);
        Objects.requireNonNull(car);
        car.removeCategory(category);
        carDao.update(car);
    }
}
