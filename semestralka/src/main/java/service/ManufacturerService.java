package service;


import dao.CarDao;
import dao.ManufacturerDao;
import model.Manufacturer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

@Service
public class ManufacturerService {

    private final ManufacturerDao dao;

    private final CarDao carDao;

    @Autowired
    public ManufacturerService(ManufacturerDao dao, CarDao carDao) {
        this.dao = dao;
        this.carDao = carDao;
    }

    @Transactional(readOnly = true)
    public List<Manufacturer> findAll() {
        return dao.findAll();
    }

    @Transactional(readOnly = true)
    public Manufacturer find(Integer id) {
        return dao.find(id);
    }

    @Transactional
    public void persist(Manufacturer manufacturer) {
        Objects.requireNonNull(manufacturer);
        dao.persist(manufacturer);
    }

}
