package service;

import dao.CarDao;
import dao.RentDao;
import model.Car;
import model.Rent;
import model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.List;
import java.util.Objects;

@Service
public class RentService {

    private final RentDao dao;
    private final CarDao carDao;

    @Autowired
    public RentService(RentDao dao, CarDao carDao) {
        this.dao = dao;
        this.carDao = carDao;
    }


    @Transactional
    public Rent create(Car car, User user, LocalDate dateTo) {
        LocalDate dateFrom = LocalDate.now();
        LocalDate dateFromparse = LocalDate.of(dateFrom.getYear(),dateFrom.getMonth(),dateFrom.getDayOfMonth());
        if(!car.isRented() && dateFrom.isBefore(dateTo)){
            Objects.requireNonNull(car);
            final Rent rent = new Rent(car,user,dateFromparse,dateTo);
            car.setRented(true);
            carDao.update(car);
            rent.setCreated(dateFromparse);
            Period period = Period.between(dateFrom, dateTo);
            int diff = period.getDays() + period.getMonths() * 30 + period.getYears() * 365;
            rent.setPrice(diff*car.getPrice());
            dao.persist(rent);
            return rent;
        }
        return null;
    }

    @Transactional
    public void persist(Rent rent) {
        dao.persist(rent);
    }

    @Transactional
    public Rent end(Rent rent) {
        Objects.requireNonNull(rent);
        final Car car = rent.getCar();
        car.setRented(false);
        rent.setCanceled(true);
        carDao.update(car);
        dao.update(rent);
        return rent;
    }

    @Transactional(readOnly = true)
    public Rent find(Integer id) {
        return dao.find(id);
    }

    @Transactional(readOnly = true)
    public List<Rent> findAll(){
        return dao.findAll();
    }
}
