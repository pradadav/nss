package service;

import dao.CarDao;
import model.Car;
import model.Category;
import model.Manufacturer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

@Service
public class CarService {

    private final CarDao dao;

    @Autowired
    public CarService(CarDao dao) {
        this.dao = dao;
    }

    @Transactional(readOnly = true)
    public List<Car> findAll() {
        return dao.findAll();
    }

    @Transactional(readOnly = true)
    public List<Car> findAll(Category category) {
        return dao.findAll(category);
    }

    @Transactional(readOnly = true)
    public List<Car> findAllMan(Manufacturer manufacturer) {
        return dao.findAll(manufacturer);
    }

    @Transactional(readOnly = true)
    public Car find(Integer id) {
        return dao.find(id);
    }

    @Transactional
    public void persist(Car car) {
        dao.persist(car);
    }

    @Transactional
    public void update(Car car) {
        dao.update(car);
    }

    @Transactional
    public void remove(Car car) {
        Objects.requireNonNull(car);
        car.setRemoved(true);
        dao.update(car);
    }
}
