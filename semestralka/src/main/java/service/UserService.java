package service;


import dao.UserDao;
import model.Category;
import model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import util.Constants;

import java.util.List;
import java.util.Objects;

@Service
public class UserService {

    private final UserDao dao;

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(UserDao dao, PasswordEncoder passwordEncoder) {
        this.dao = dao;
        this.passwordEncoder = passwordEncoder;
    }

    @Transactional(readOnly = true)
    public User find(Integer id) {
        return dao.find(id);
    }

    @Transactional(readOnly = true)
    public List<User> findAll() {
        return dao.findAll();
    }

    @Transactional
    public void persist(User user) {
        Objects.requireNonNull(user);
        while(this.find(user.getId())!=null){
            user.setId(user.getId()+1);
        }
        if (user.getUsername().matches(".*[a-zA-Z]+.*") && user.getPassword().matches(".*[a-zA-Z]+.*") && user.getFirstName().matches(".*[a-zA-Z]+.*") && user.getLastName().matches(".*[a-zA-Z]+.*")){
            user.encodePassword(passwordEncoder);
            if (user.getRole() == null) {
                user.setRole(Constants.DEFAULT_ROLE);
            }
            dao.persist(user);
        }
    }

    @Transactional
    public void delete(User user) {
        Objects.requireNonNull(user);
        dao.deleteByUsername(user.getUsername());
    }


    @Transactional(readOnly = true)
    public boolean exists(String username) {
        return dao.findByUsername(username) != null;
    }


}
