function drawNavbar() {


    $.get("./header.html", function(data){
        $("#header").replaceWith(data);
    });

    $.get("./navbar.html", function(data){
        $("#navigation").replaceWith(data);
    });

}

function getRents(){
    // price, car, user, date_to, date_from
    $.ajax({
        url: "rest/rents/"
    }).then(function(response) {

        for(var i =0;i <= response.length-1;i++)
        {
            var item = response[i];

            if (item.canceled){
                var data = '                <div class="row mt-3">\n' +
                    '                    <div class="col-8 offset-2 bg-light">\n' +
                    '                        <div class="row shadow-lg">\n' +
                    '                            <div class="col">\n' +
                    '                                <h4 class="mt-4">'+item.date_from+' - '+item.date_to+'</h4>\n' +
                    '                                <h5 class="pb-3">'+item.car.manufacturer.name+' '+item.car.model+'</h5>\n' +
                    '                            </div>\n' +
                    '                            <div class="col-4 offset-3">\n' +
                    '                                <h4 class="text-center mt-2">'+item.price+' CZK</h4>\n' +
                    '                            </div>\n' +
                    '                        </div>\n' +
                    '                    </div>\n' +
                    '                </div>'+
                    '<div class="rentslist"></div>'
                $('.rentslist').replaceWith(data);
            } else {
                var data = '                <div class="row mt-3">\n' +
                    '                    <div class="col-8 offset-2 bg-light">\n' +
                    '                        <div class="row shadow-lg">\n' +
                    '                            <div class="col">\n' +
                    '                                <h4 class="mt-4">'+item.date_from+' - '+item.date_to+'</h4>\n' +
                    '                                <h5 class="pb-3">'+item.car.manufacturer.name+' '+item.car.model+'</h5>\n' +
                    '                            </div>\n' +
                    '                            <div class="col-4 offset-3">\n' +
                    '                                <h4 class="text-center mt-2">'+item.price+' CZK</h4>\n' +
                    '                                <button class="btn btn-primary btn-lg btn-block mt-1" type="submit" onclick="cancelRent('+item.id+')">Cancel</button>\n' +
                    '                            </div>\n' +
                    '                        </div>\n' +
                    '                    </div>\n' +
                    '                </div>'+
                    '<div class="rentslist"></div>'
                $('.rentslist').replaceWith(data);
            }


        }
    });
}


function cancelRent(x) {
    $.ajax({
        type: "DELETE",
        url: "rest/rents/"+x+"",
        success: function(){
            alert("success");},
        failure: function(errMsg) {
            alert(errMsg);
        }
    });
    location.reload();
}

function deleteUser(x) {
    $.ajax({
        type: "DELETE",
        url: "rest/users/"+x+"",
        success: function(){
            alert("success");},
        failure: function(errMsg) {
            alert(errMsg);
        }
    });
    location.reload();
}


function rent(x) {

    var carid = x;
    console.log(carid);
    var elId = "date"+x+""
    console.log(elId);
    var dateTo = document.getElementById(elId).value;
    var res = dateTo.split(".", 3);
    var day = res[0];
    var month = res[1];
    var year = res[2];
    console.log(day, month, year);

    var rent = {
        "carId": carid,
        "day": day,
        "month": month,
        "year": year
    }

    $.ajax({
        type: "POST",
        url: "rest/rents/",
        data: JSON.stringify(rent),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(){
            alert("success");},
        failure: function(errMsg) {
            alert(errMsg);
        }
    });
    location.reload();
}

function getCars(){
    $.ajax({
        url: "rest/cars/"
    }).then(function(response) {

        for(var i =0;i <= response.length-1;i++)
        {
            var item = response[i];

            var categoryString = "";

            for (var j =0;j <= item.categories.length-1;j++){
                var itemsec = item.categories[j];


                if (j != item.categories.length - 1){
                    categoryString += itemsec.name;
                    categoryString += ", ";

                }else {
                    categoryString += itemsec.name;
                }
            }


            var data = '<div class="row mt-3">\n' +
                '    <div class="col-8 offset-2 bg-light">\n' +
                '        <div class="row shadow-lg">\n' +
                '            <div class="col-4">\n' +
                '                <h4 class="mt-2 name">'+item.manufacturer.name+' '+ item.model  +' </h4>\n' +
                '                <p class="seats mt-4">Seats: '+ item.seats +'</p>\n' +
                '                <p class="category mt-4 mb-0">'+categoryString+'</p>\n' +
                '            </div>\n' +
                '            <div class="col-4 offset-4">\n' +
                '                <h4 class="text-center mt-2 price">'+item.price+' CZK / day</h4>\n' +
                '                <input type="text" class="form-control" id="date'+item.id+'" placeholder="Date to" required="">' +
                '                <button class="btn btn-primary btn-lg btn-block my-2 rent" type="submit" onclick="rent('+item.id+')">Rent</button>\n' +
                '            </div>\n' +
                '        </div>\n' +
                '    </div>\n' +
                '</div>\n' +
                '<div class="carlist"></div>'

            $('.carlist').replaceWith(data);

        }

    });
}

function login (){

    var inputUsername = document.getElementById("username").value;
    var inputPassword = document.getElementById("password").value;

    $.post('j_spring_security_check', { username: inputUsername, password : inputPassword},
        function(returnedData){
            console.log(returnedData);
    });
}

// function register (){
//
//     var inputUsername = document.getElementById("username").value;
//     var inputPassword = document.getElementById("password").value;
//     var inputFirstName = document.getElementById("firstName").value;
//     var inputLastName = document.getElementById("lastName").value;
//     var inputMail = document.getElementById("email").value;
//
//     $.post('rest/users', { firstName: inputFirstName, lastName : inputLastName, username: inputUsername, email: inputMail, password: inputPassword },
//         function(returnedData){
//             console.log(returnedData);
//         });
// }

function logout (){

    $.post('j_spring_security_logout',
        function(returnedData){
            console.log(returnedData);
        });
}


function register(){

    var firstname = document.getElementById("firstname").value;
    var lastname = document.getElementById("lastname").value;
    var username = document.getElementById("username").value;
    var password = document.getElementById("password").value;

    var user = {
        "id": 100,
        "firstName": ""+firstname+"",
        "lastName": ""+lastname+"",
        "username": ""+username+"",
        "password": ""+password+"",
        "role": "CUSTOMER"
    }

    $.ajax({
        type: "POST",
        url: "rest/users/",
        data: JSON.stringify(user),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(){
            alert("success");},
        failure: function(errMsg) {
            alert(errMsg);
        }
    });
    window.location.replace("./index.html");
}


function getUsers(){
    $.ajax({
        url: "rest/users/"
    }).then(function(response) {

        for(var i =0;i <= response.length-1;i++)
        {
            var item = response[i];

            var data = '                <div class="row mt-3">\n' +
                '                    <div class="col-8 offset-2 bg-light">\n' +
                '                        <div class="row shadow-lg">\n' +
                '                            <div class="col">\n' +
                '                                <h4 class="mt-2 mb-1">'+item.firstName+' '+item.lastName+'</h4>\n' +
                '                                <p class="mb-1">Username: '+item.username+'</p>\n' +
                '                                <p class="mb-2">Role: '+item.role+'</p>\n' +
                '                            </div>\n' +
                '                            <div class="col-3 offset-3">\n' +
                '                            <button class="btn btn-primary btn-lg btn-block mt-4 rent" type="submit" onclick="deleteUser('+item.id+')">Delete</button>\n' +
                '                            </div>' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                </div>' +
                '<div class="userlist"></div>'

            $('.userlist').replaceWith(data);

        }

    });
}